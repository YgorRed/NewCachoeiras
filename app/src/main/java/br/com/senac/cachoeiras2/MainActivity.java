package br.com.senac.cachoeiras2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

public static final String CACHOEIRA = "cachoeira";
public static final String IMAGEM = "imagem";
private List<ClasseCachoeira> lista = new ArrayList<>();
private ArrayAdapter<ClasseCachoeira> adapter;
List<ClasseCachoeira> listacachoeira = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      final  ListView listview = findViewById(R.id.listadecachoeiras) ;
      int layout = android.R.layout.simple_list_item_1;
      adapter = new ArrayAdapter<ClasseCachoeira>(this, layout,listacachoeira);
      listview.setAdapter(adapter);

      listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

          @Override
          public void onItemClick(AdapterView<?> adapterView, View view, int posicao, long l) {
              ClasseCachoeira Cachoeira = (ClasseCachoeira) adapterView.getItemAtPosition(posicao);
              Intent intent = new Intent(MainActivity.this,DetalheActivity.class);

              intent.putExtra(CACHOEIRA,Cachoeira);

              intent.putExtra(IMAGEM,Cachoeira.getImagem());

              startActivity(intent);
          }
      }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    public void novo(){


    }

}
