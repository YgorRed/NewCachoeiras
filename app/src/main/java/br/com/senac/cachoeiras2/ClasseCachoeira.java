package br.com.senac.cachoeiras2;


import android.graphics.Bitmap;

import java.io.Serializable;

public class ClasseCachoeira implements Serializable {

    private String nome;
    private String Informacoes;
    private float classificacao;
    private  transient Bitmap imagem;
    private int id ;

    public ClasseCachoeira(String nome, String informacoes, float classificacao, Bitmap imagem, int id) {
        this.nome = nome;
        Informacoes = informacoes;
        this.classificacao = classificacao;
        this.imagem = imagem;
        this.id = id;
    }

    public ClasseCachoeira() {

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getInformacoes() {
        return Informacoes;
    }

    public void setInformacoes(String informacoes) {
        Informacoes = informacoes;
    }

    public float getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(float classificacao) {
        this.classificacao = classificacao;
    }

    public Bitmap getImagem() {
        return imagem;
    }

    public void setImagem(Bitmap imagem) {
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
